package systeme_electoral.sondages;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;
import systeme_electoral.scrutins.ScrutinUnTour;

import java.util.*;
import java.lang.Math;
import java.util.stream.Collectors;


public class Sondage{

    private final ArrayList<Electeur> tabElecteursSondage;
    private final int tailleEchantillon;
    //Niveau de confiance et variable centrée-réduite associée
    //selon la précision de l'échantillon souhaitée
    private final HashMap<Double, Double> precisions = new HashMap(){{
        put(0.80, 1.28);
        put(0.85, 1.44);
        put(0.90, 1.65);
        put(0.95, 1.96);
        put(0.99, 2.58);
    }};

    public Sondage(int taillePopulation)
    {
        double precision = 0.95;    //Choix du niveau de confiance pour la HashMap
        double ecartType = 0.5;

        this.tabElecteursSondage = new ArrayList<>();

        //Calcul de l'échantillon significatif
        double numerateur1 = Math.pow(precisions.get(precision), 2) * ecartType * (1 - ecartType);
        double numerateurTotal = numerateur1 / Math.pow(1 - precision, 2);
        double fraction = numerateurTotal / taillePopulation;

        tailleEchantillon =    (int) Math.round( numerateurTotal / (1 + fraction) );
    }

    /**
     * Effectuer un sondage en triant les candidats via un scrutin à un tour
     * @param candidats Liste des candidats du futur scrutin
     * @param electeurs Liste des électeurs du futur scrutin
     */
    public void effectuerSondage(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs)
    {
        Random rnd = new Random();
        ArrayList<Integer> tabInd = new ArrayList<>();
        //System.out.println(tailleEchantillon);
        for (int i=0; i<tailleEchantillon; i++)
        {
            //On récupère un électeur aléatoirement qui n'a
            //pas encore été récupéré
            int ind;
            do
            {
                ind = rnd.nextInt(electeurs.size());
            } while (tabInd.contains(Integer.valueOf(ind)));

            tabInd.add(Integer.valueOf(ind));
            tabElecteursSondage.add(electeurs.get(ind));
        }
        //On effectue un scrutin un tour pour classer les candidats
        ScrutinUnTour scrutinDeSondage = new ScrutinUnTour();
        scrutinDeSondage.effectuerScrutin(candidats, tabElecteursSondage);
    }

    /**
     * Sondage de déplacement de l'électeur vers le candidat le plus proche
     * @param candidats Liste des candidats du futur scrutin
     * @param electeurs Liste des électeurs du futur scrutin
     */
    public void lectureDuSondageDeplacementProche(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs) {
        for(Electeur e : electeurs) {
            //On trie les candidats selon la proximité avec l'électeur
            candidats.sort(Comparator.comparing(e::comparerAxes));
            //L'électeur interagit avec le premier candidat
            e.interactionSocioPolitique(candidats.get(0));
        }
    }

    /**
     * Sondage de déplacement de l'électeur vers le candidat le plus utile
     * @param candidats Liste des candidats du futur scrutin
     * @param electeurs Liste des électeurs du futur scrutin
     */
    public void lectureDuSondageDeplacementUtile(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs) {
        for(Electeur e : electeurs) {
            HashMap<Double, Candidat> utiliteParCandidat = new HashMap<>();

            //On calcule l'utilité du candidat selon l'électeur
            for(Candidat c : candidats) {
                double preference = 1 / e.comparerAxes(c);
                double pourcentage = c.getNbVotes() / tailleEchantillon;
                double utilite = preference * pourcentage;
                utiliteParCandidat.put(utilite, c);
            }

            // On prend l'utilité max de la HashMap
            // (on récupère le max d'une List des clés de la HashMap)
            Double max = Collections.max(  utiliteParCandidat.keySet().stream().collect(Collectors.toList())  );
            //L'électeur interagit avec le candidat à l'utilité maximale
            e.interactionSocioPolitique(utiliteParCandidat.get(max));

        }
    }

    /**
     * Sondage de déplacement de l'électeur vers le candidat le plus utile
     * proportionnellement à tous les candidats disponibles
     * @param candidats Liste des candidats du futur scrutin
     * @param electeurs Liste des électeurs du futur scrutin
     */
    public void lectureDuSondageDeplacementUtileProportionnel(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs) {
        double utiliteMax = 100.0;  //On fixe l'utilité max pour ramener la valeur entre 0 et 1 ensuite

        for(Electeur e : electeurs) {
            for(Candidat c : candidats) {
                //On calcule l'utilité du candidat
                double preference = 1 / e.comparerAxes(c);
                double pourcentage = c.getNbVotes() / tailleEchantillon;
                double utilite = preference * pourcentage;

                //Si le candidat n'est pas trop proche (car cela ne sert à rien de changer l'électeur)
                //L'électeur change son opinion proportionellement aux autres
                if(utilite < utiliteMax)
                    e.interactionProportionnelle(utilite, utiliteMax);
            }
        }
    }
}
