package systeme_electoral.scrutins;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;

import java.util.ArrayList;

public abstract class Scrutin {

    /**
     * Méthode de vote commune à chaque sondage
     * @param c le candidat recevant les points
     * @param points Le nombre de points pour chaque candidat
     */
    public void voter(Candidat c, int points) {
        c.addVotes(points);
    }

    /**
     * Méthode de lancement du scrutin sélectionné
     * @param candidats ArrayList des candidats au scrutin
     * @param electeurs ArrayList des électeurs du scrutin
     */
    public abstract void effectuerScrutin(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs);
}
