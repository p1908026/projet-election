package systeme_electoral.scrutins;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Random;

public class VoteApprobation extends Scrutin {

    public VoteApprobation() {
        super();
    }

    public void effectuerScrutin(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs){
        for(Electeur e : electeurs) {
            //On trie les candidats selon la proximité avec l'électeur
            candidats.sort(Comparator.comparing(e::comparerAxes));

            Random rnd = new Random();
            //Chaque électeur vote pour un nombre aléatoire de candidats
            int nbcandidatsavoter = rnd.nextInt(candidats.size());
            for(int i=0; i < nbcandidatsavoter; i++) {
                voter(candidats.get(i), 1);
            }

        }

    }

}
