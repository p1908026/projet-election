package systeme_electoral.scrutins;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;

import java.util.*;

public class ScrutinDeuxTours extends Scrutin {

    public ScrutinDeuxTours() {
        super();
    }

    public void effectuerScrutin(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs){
        //Les électeurs trient les candidats selon leurs préférences et votent pour le premier
        electeursVotent(candidats, electeurs);

        //S'il y a au moins 2 candidats
        if(candidats.size() > 2) {
            System.out.println("##########Resultat 1e tour##########");
            //On trie les candidats par nombre de votes reçus
            candidats.sort(Comparator.comparing(Candidat::getNbVotes, Collections.reverseOrder()));
            for (Candidat c : candidats) {
                //Candidat personnalisé du nom de Andre à la position 0
                String nomCandidat = (c.getId() == 0) ? "Andre " : "Candidat ";
                System.out.println(nomCandidat + c.getId() + ": " + c.getNbVotes());
            }

            //On conserve uniquement les deux premiers candidats
            ArrayList<Candidat> newTabCandidats = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                newTabCandidats.add(candidats.get(i));
            }
            candidats.clear();
            candidats.addAll(newTabCandidats);

            System.out.println("##########Resultat 2e tour##########");

            //Chaque candidat reçoit 0 votes à l'initialisation
            for (Candidat c : candidats) {
                c.setNbVotes(0);
            }

            //Les électeurs trient les candidats selon leurs préférences et votent pour le premier
            electeursVotent(candidats, electeurs);
        }
    }

    /**
     * Chaque électeur vote pour le candidat vers qui il se sent le plus proche
     * @param candidats Liste des candidats du scrutin
     * @param electeurs Liste des électeurs du scrutin
     */
    private void electeursVotent(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs){
        for(Electeur e : electeurs) {
            //On trie les candidats selon la proximité avec l'électeur
            candidats.sort(Comparator.comparing(e::comparerAxes));
            //Le candidat vote pour le premier de la liste
            voter(candidats.get(0), 1);
        }
    }
}
