package systeme_electoral.scrutins;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class VoteAlternatif extends Scrutin {

    //Nombre de tours effectués lors du vote
    int nbTour = 1;

    public VoteAlternatif() {
        super();
        nbTour = 1;
    }

    public void effectuerScrutin(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs){
        //Chaque candidat reçoit 0 votes à l'initialisation
        for (Candidat c : candidats) {
            c.setNbVotes(0);
        }

        System.out.println("##########Resultat " + nbTour + "e tour##########");
        //S'il ne reste qu'un candidat
        if(candidats.size() == 1) {
            for (int i=0; i<electeurs.size() ; i++) {
                //Chaque électeur vote pour lui
                voter(candidats.get(0), 1);
            }

            //Sinon, on trie les candidats restant selon l'électeur
        } else {
            for(Electeur e : electeurs) {
                //On trie les candidats selon la proximité avec l'électeur
                candidats.sort(Comparator.comparing(e::comparerAxes));
                //Le candidat vote pour le premier de la liste
                voter(candidats.get(0), 1);
            }

            //On trie les candidats par nombre de votes reçus
            candidats.sort(Comparator.comparing(Candidat::getNbVotes, Collections.reverseOrder()));

            for (Candidat c : candidats) {
                //Candidat personnalisé du nom de Andre à la position 0
                String nomCandidat = (c.getId() == 0) ? "Andre " : "Candidat ";
                System.out.println(nomCandidat + c.getId() + ": " + c.getNbVotes());
            }

            // On enlève le dernier candidat et réitère un tour de vote
            candidats.remove(candidats.size() - 1); nbTour++;
            effectuerScrutin(candidats, electeurs);
        }
    }

}
