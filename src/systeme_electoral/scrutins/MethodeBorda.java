package systeme_electoral.scrutins;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;

import java.util.ArrayList;
import java.util.Comparator;

public class MethodeBorda extends Scrutin {

    public MethodeBorda() {
        super();
    }

    @Override
    public void effectuerScrutin(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs) {
        for(Electeur e : electeurs) {
            //On trie les candidats selon la proximité avec l'électeur
            candidats.sort(Comparator.comparing(e::comparerAxes));
            int n = candidats.size();   //Max de points à donner => nombre de candidats

            for(Candidat c : candidats) {
                //On donne un nombre de points décroissant à chaque candidat
                voter(c,n--);
            }
        }
    }
}
