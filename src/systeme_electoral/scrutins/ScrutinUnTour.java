package systeme_electoral.scrutins;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;

import java.util.*;

public class ScrutinUnTour extends Scrutin {

    public ScrutinUnTour() {
        super();
    }

    public void effectuerScrutin(ArrayList<Candidat> candidats, ArrayList<Electeur> electeurs){
        for(Electeur e : electeurs) {
            //On trie les candidats selon la proximité avec l'électeur
            candidats.sort(Comparator.comparing(e::comparerAxes));
            //Le candidat vote pour le premier de la liste
            voter(candidats.get(0), 1);
        }
    }
}
