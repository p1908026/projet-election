package systeme_electoral;

import systeme_electoral.personnes.Candidat;
import systeme_electoral.personnes.Electeur;
import systeme_electoral.personnes.Population;
import systeme_electoral.scrutins.*;
import systeme_electoral.sondages.Sondage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class SystemeElectoral extends Observable {
    private final ArrayList<Electeur> tabElecteurs;
    private final ArrayList<Candidat> tabCandidatsNEPASCHANGER;
    private final ArrayList<Candidat> tabCandidatsQuiChange;
    private Scrutin scrutin;
    private Sondage sondage;

    //Valeurs par défaut
    private int nbCandidats = 2;   //Valeur minimale
    private int nbElecteurs = 1;   //Valeur minimale
    private String[] axes = new String[]{"Ecologie", "Pouvoir d'achat"};

    public SystemeElectoral(String file) throws IOException {
        try {
            initialiser(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Initialisation des candidats et des électeurs
        this.tabElecteurs = new ArrayList<>();
        this.tabCandidatsNEPASCHANGER = new ArrayList<>();  //Liste complète des candidats
        this.tabCandidatsQuiChange = new ArrayList<>(); //Liste des candidats modifiée par les votes

        //Initialisation des candidats
        for(int i = 0; i < nbCandidats; i++) {
            Candidat c = new Candidat();
            tabCandidatsNEPASCHANGER.add(c);
            c.addAxes(axes);
            c.donnerAxes();
            //System.out.println(c.getAxes());
        }

        //Initialisation des électeurs
        for(int i = 0; i < nbElecteurs; i++) {
            Electeur e = new Electeur();
            tabElecteurs.add(e);
            e.addAxes(axes);
            e.donnerAxes();
        }
    }

    /**
     * Récupérer les valeurs du fichier de configuration
     * @param file Chemin vers le fichier
     * @throws IOException Excepton lors de la lecture du fichier
     */
    public void initialiser(String file) throws IOException {
        List<String> strfile = Files.readAllLines((Paths.get(file)));

        //Récupération dans le fichier de configuration
        for(String ligne : strfile){
            if(!ligne.startsWith("//")){
                String[] ligneTab = ligne.split(":");

                //Récupérer le nombre de candidats
                if (ligneTab[0].equals("candidats")){
                    if(ligneTab.length > 1){
                        int newCandidats = Math.abs(Integer.parseInt(ligneTab[1]));
                        if(newCandidats > nbCandidats)
                            nbCandidats = newCandidats;
                    }

                    //Récupérer le nombre d'électeurs
                } else if (ligneTab[0].equals("electeurs")){
                    if(ligneTab.length > 1){
                        int newElecteurs = Math.abs(Integer.parseInt(ligneTab[1]));
                        if(newElecteurs > nbElecteurs)
                            nbElecteurs = newElecteurs;
                    }

                    //Récupérer les axes
                } else if (ligneTab[0].equals("axes")){

                    if(ligneTab.length > 1 && ligneTab[1].split(",").length > 0){
                        String[] axesTabVal = ligneTab[1].split(",");
                        axes = new String[axesTabVal.length];

                        for(int i=0; i < axesTabVal.length; i++){
                            axes[i] = axesTabVal[i];
                        }
                    }

                }
            }
        }
        //System.out.println(axes[0]);
    }

    /**
     * Effectuer le scrutin choisi par l'utilisateur
     * @param s Numéro du scrutin
     */
    public void faireScrutin(int s){
        //Réinitialiser la liste de candidats utilisée pour le vote
        tabCandidatsQuiChange.clear();
        tabCandidatsQuiChange.addAll(tabCandidatsNEPASCHANGER);

        switch (s) {
            case 1: //Scrutin à un tour
                this.scrutin = new ScrutinUnTour();
                scrutin.effectuerScrutin(tabCandidatsQuiChange, tabElecteurs);
                afficherResultat();
                break;

            case 2: //Scrutin à deux tours
                this.scrutin = new ScrutinDeuxTours();
                scrutin.effectuerScrutin(tabCandidatsQuiChange, tabElecteurs);
                afficherResultat();
                break;

            case 3: //Vote par approbation
                this.scrutin = new VoteApprobation();
                scrutin.effectuerScrutin(tabCandidatsQuiChange, tabElecteurs);
                afficherResultat();
                break;

            case 4: //Vote alternatif
                this.scrutin = new VoteAlternatif();
                scrutin.effectuerScrutin(tabCandidatsQuiChange, tabElecteurs);
                afficherResultat();
                break;

            case 5: //Vote via méthode Borda
                this.scrutin = new MethodeBorda();
                scrutin.effectuerScrutin(tabCandidatsQuiChange, tabElecteurs);
                afficherResultat();
                break;

            default:
                System.out.println("null");
                break;
        }
    }

    /**
     * Effectuer l'interaction choisie par l'utilisateur
     * @param i Numéro d'interaction
     */
    public void faireInteraction(int i){
        //Réinitialiser la liste de candidats utilisée pour le vote
        tabCandidatsQuiChange.clear();
        tabCandidatsQuiChange.addAll(tabCandidatsNEPASCHANGER);

        //Interaction socio-politique simple
        if(i == 1) {
            //Récupérer l'ensemble de la population (candidats + électeurs)
            ArrayList<Population> tabPopulation = new ArrayList<>();
            tabPopulation.addAll(tabCandidatsNEPASCHANGER);
            tabPopulation.addAll(tabElecteurs);

            Random rnd = new Random();

            //Chaque électeur va interagir 20 fois avec une personnes aléatoire
            for (Electeur e : tabElecteurs){
                for (int j=0; j<20; j++){
                    Population p;
                    int ind;
                    do {
                        ind = rnd.nextInt(tabPopulation.size());
                        p = tabPopulation.get(ind);

                    } while(e.getId() == p.getId());

                    e.interactionSocioPolitique(p);
                }
            }

        } else {

            //Effectuer un sondage au préalable
            this.sondage = new Sondage(tabElecteurs.size());
            this.sondage.effectuerSondage(tabCandidatsQuiChange, tabElecteurs);

            afficherClassement();

            //On trie les candidats par nombre de votes reçus
            tabCandidatsQuiChange.sort(Comparator.comparing(Candidat::getNbVotes, Collections.reverseOrder()));

            // Ne garder que les 5 premiers candidats au plus
            int limite = 5; //(tabCandidatsQuiChange.size() >= 5) ? 5 : tabCandidatsQuiChange.size();

            switch (i) {
                case 2: //Sondage de déplacement vers le plus proche
                    this.sondage.lectureDuSondageDeplacementProche((ArrayList<Candidat>) tabCandidatsQuiChange.stream().limit(limite).collect(Collectors.toList()), tabElecteurs);
                    break;

                case 3: //Sondage de déplacement vers le plus utile
                    this.sondage.lectureDuSondageDeplacementUtile((ArrayList<Candidat>) tabCandidatsQuiChange.stream().limit(limite).collect(Collectors.toList()), tabElecteurs);
                    break;

                case 4: //Sondage de déplacement vers le plus utile proportionnellement à tous
                    this.sondage.lectureDuSondageDeplacementUtileProportionnel((ArrayList<Candidat>) tabCandidatsQuiChange.stream().limit(limite).collect(Collectors.toList()), tabElecteurs);
                    break;

                default:
                    System.out.println("null");
                    break;
            }
            afficherClassement();
        }

        affichageEvolution();
    }

    /**
     * Affiche le résultat du vote dans la console
     */
    public void afficherResultat() {
        //Afficher le classement des candidats
        afficherClassement();

        System.out.println("########################################");
        System.out.println("!!!!!!!!Victoire pour Candidat " + tabCandidatsQuiChange.get(0).getId() + "!!!!!!!!");
        System.out.println("########################################");
    }

    /**
     * Affiche le classement des candidats
     */
    public void afficherClassement() {
        //On trie les candidats par nombre de votes reçus
        tabCandidatsQuiChange.sort(Comparator.comparing(Candidat::getNbVotes, Collections.reverseOrder()));

        //Afficher chaque candidat et les votes reçus
        for(Candidat c : tabCandidatsQuiChange) {
            //Candidat personnalisé du nom de Andre à la position 0
            String nomCandidat = (c.getId() == 0) ? "Andre " : "Candidat ";
            System.out.println(nomCandidat+ c.getId() + ": " + c.getNbVotes());
        }
    }

    /**
     * Afficher l'évolution des opinions totales avant et après un sondage
     */
    public void affichageEvolution() {
        //Récupérer le nombre total d'axes
        int sizeAxes = tabElecteurs.get(0).getAxes().keySet().size();
        Double[] axeAvant = new Double[sizeAxes];
        Double[] axeApres = new Double[sizeAxes];
        //Initialiser les tableaux de totaux
        for(int i=0; i<sizeAxes; i++){
            axeAvant[i] = 0.0;
            axeApres[i] = 0.0;
        }

        //Récupérer le total des valeurs de chaque axe des électeurs
        for(Electeur e : tabElecteurs) {
            int i = 0;
            for(String value : e.getAxes().keySet()) {
                //Valeur d'axe avant sondage
                axeAvant[i] += e.getAxes().get(value).get(e.getAxes().get(value).size() - 2);
                //Valeur d'axe après sondage
                axeApres[i] += e.getAxes().get(value).get(e.getAxes().get(value).size() - 1);
                i++;
            }
            //System.out.println(e.getAxes());
        }

        //Afficher l'évolution du total des axes
        for(String axe : tabElecteurs.get(0).getAxes().keySet()){
            int i = 0;
            System.out.println("########################################");
            System.out.print("Evolution de l'axe '"+axe+"' : ");
            System.out.println(axeAvant[i] + " => " + axeApres[i]);
            System.out.println("########################################");
            i++;
        }
    }

    public void resetVotes() {
        for(Candidat c : tabCandidatsNEPASCHANGER) {
            c.setNbVotes(0);
        }
    }

    public int getNBCandidats() {
        return tabCandidatsNEPASCHANGER.size();
    }

    public int getNBElecteurs() {
        return tabElecteurs.size();
    }
}
