package systeme_electoral.personnes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class Population {

    private final int id;
    private static int dernierid = 0;
    /**
     * Les axes sont stockés de la manière suivante :
     * Nom_axe -> Tableau de valeurs d'axes
     * Pour ainsi garder les anciennes valeurs des axes dans le cas où un sondage est effectué
     * pour pouvoir les comparer
     */
    private final HashMap<String, ArrayList<Double>> axes;

    /**
     * Affectation d'un id unique à la personnes
     * et création de sa HashMap d'axes
     */
    public Population() {
        id = dernierid++;
        axes = new HashMap<>();
    }

    /**
     * Fixe les noms des axes de la personnes
     * @param tabAxes Tableau des axes à donner
     */
    public void addAxes(String[] tabAxes) {
        for (String s : tabAxes) {
            axes.put(s, new ArrayList<>());
        }
    }

    /**
     * Affecte des valeurs aléatoires à chaque axe
     */
    public void donnerAxes(){
        for (ArrayList<Double> value : axes.values()) {
            Random rnd = new Random();
            double a = rnd.nextDouble();
            value.add(a);
        }
    }

    public int getId() {
        return id;
    }

    public HashMap<String, ArrayList<Double>> getAxes() {
        return axes;
    }

    /**
     * Compare les axes de deux personnes pour déterminer s'il sont proches
     * @param p Personne avec laquelle on compare les axes
     * @return La différence globale entre leurs axes
     */
    public double comparerAxes(Population p) {
        double diff = 0;
        //On rajoute la différence entre les axes respectifs
        for (String value : axes.keySet()) {
            diff += Math.abs(this.axes.get(value).get(this.axes.get(value).size() - 1) - p.getAxes().get(value).get(p.getAxes().get(value).size() - 1));
        }
        if(diff == 0) diff = 0.00000000000000001; //Pour éviter de diviser par 0 pour une interaction

        //Plus diff est petit, plus ils sont proches
        return diff;
    }

    /**
     * Compare les axes d'une personnes avec un rapport pour déterminer
     * si elle est proche de ce dernier
     * @param axe Valeur d'axe issue d'une utilité calculée
     * @return La différence globale entre les axes de la personnes et le rapport
     */
    public double comparerAxes(double axe) {
        double diff = 0;
        //On rajoute la différence entre les axes respectifs
        for (String value : axes.keySet()) {
            diff += Math.abs(this.axes.get(value).get(this.axes.get(value).size() - 1) - axe);
        }
        if(diff == 0) diff = 0.00000000000000001; //Pour éviter de diviser par 0 pour une interaction

        //Plus diff est petit, plus ils sont proches
        return diff;
    }

    /**
     * Effectue une interaction socio-politique entre deux personnes
     * @param p Personne cible de l'interaction
     */
    public void interactionSocioPolitique(Population p){
        //Fixation des seuls min et max
        double seuilmin = axes.size()/4.0;
        double seuilmax = axes.size()/2.0;

        double comparaisonAxe = comparerAxes(p);    //Comparer les axes des personnes

        //Si le rapport est en dessous du seuil minimum
        if(comparaisonAxe < seuilmin) {

            //On rajoute pour chaque axe une nouvelle valeur augmentée
            for(String value : axes.keySet()) {
                double newAxe = this.axes.get(value).get(this.axes.get(value).size() - 1) - p.getAxes().get(value).get(p.getAxes().get(value).size() - 1);
                double newValue = this.axes.get(value).get(this.axes.get(value).size() - 1) + newAxe * 0.1;
                this.axes.get(value).add( Math.max(0, Math.min(newValue, 1)) );
            }
        }

        //Si le rapport est au dessus du seuil maximum
        if(comparaisonAxe > seuilmax) {

            //On rajoute pour chaque axe une nouvelle valeur diminuée
            for(String value : axes.keySet()) {
                double newAxe = this.axes.get(value).get(this.axes.get(value).size() - 1) - p.getAxes().get(value).get(p.getAxes().get(value).size() - 1);
                double newValue = this.axes.get(value).get(this.axes.get(value).size() - 1) - newAxe * 0.1;
                this.axes.get(value).add( Math.max(0, Math.min(newValue, 1)) );
            }
        }
    }

    /**
     * Change la valeur des axes de la personnes actuelle selon l'utilité calculée précédemment
     * @param utilite Utilité calculée
     * @param utiliteMax Utilité max possible
     */
    public void interactionProportionnelle(double utilite, double utiliteMax){
        double rapport = utilite / utiliteMax;  //Ramener l'utilité entre 0 et 1

        double comparaisonAxe = comparerAxes(rapport);   //Comparer les axes des selon le rapport

        //On rajoute pour chaque axe une nouvelle valeur
        for(String value : axes.keySet()) {
            //System.out.println(rapport);
            //System.out.println("Dernier axe "+ this.axes.get(value).get(this.axes.get(value).size() - 1));

            double newAxe = this.axes.get(value).get(this.axes.get(value).size() - 1) - comparaisonAxe;
            double newValue = this.axes.get(value).get(this.axes.get(value).size() - 1) + newAxe * 0.1;
            this.axes.get(value).add( Math.max(0, Math.min(newValue, 1)) );

            //System.out.println("Dernier axe nouveau "+ this.axes.get(value).get(this.axes.get(value).size() - 1));
            //System.out.println("++");
        }
        //System.out.println("-----------");
    }
}
