package systeme_electoral.personnes;

public class Candidat extends Population {

    private int _nbVotes;

    public Candidat () {
        super();
        _nbVotes = 0;
    }

    public void addVotes(int votes){
        _nbVotes += votes;
    }

    public Integer getNbVotes() {
        return _nbVotes;
    }

    public void setNbVotes(int nbVotes) {
        _nbVotes = nbVotes;
    }
}
