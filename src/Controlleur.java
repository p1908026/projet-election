import systeme_electoral.SystemeElectoral;

import javax.swing.*;

public class Controlleur {

    private final SystemeElectoral s;
    private final VueSystemeElectoral vs;
    JPanel mainPanel;

    public Controlleur(SystemeElectoral s, VueSystemeElectoral vs) {
        this.s = s;
        this.vs = vs;
        initView();
        initButtons();
    }

    private void initView() {
        vs.setContentPane(vs.getStartPane());
        vs.update();
    }

    /**
     * Définir la fonction à appeler si le bouton est pressé
     */
    private void initButtons() {
        vs.getStartBtn().addActionListener(
                e -> {
                    vs.setContentPane(vs.getChoicePane());
                    vs.update();
                }
        );
        vs.getScrutinBtn().addActionListener(
                e -> {
                    vs.setContentPane(vs.getScrutinChoicePane());
                    vs.update();
                }
        );
        vs.getInteractionBtn().addActionListener(
                e -> {
                    vs.setContentPane(vs.getInteractionChoicePane());
                    vs.update();
                }
        );
        vs.getRetourBtn().addActionListener(
                e -> {
                    vs.setContentPane(vs.getStartPane());
                    vs.update();
                }
        );
        vs.getScrutin_en_un_tour().addActionListener(
                e -> {
                    s.resetVotes();
                    s.faireScrutin(1);
                }
        );
        vs.getScrutin_en_deux_tours().addActionListener(
                e -> {
                    s.resetVotes();
                    s.faireScrutin(2);
                }
        );
        vs.getVote_approbation().addActionListener(
                e -> {
                    s.resetVotes();
                    s.faireScrutin(3);
                }
        );
        vs.getVote_alternatif().addActionListener(
                e -> {
                    s.resetVotes();
                    s.faireScrutin(4);
                }
        );
        vs.getMethode_borda().addActionListener(
                e -> {
                    s.resetVotes();
                    s.faireScrutin(5);
                }
        );
        vs.getScrutinRetourBtn().addActionListener(
                e -> {
                    vs.setContentPane(vs.getChoicePane());
                    vs.update();
                }
        );
        vs.getInteractions_sociopolitiques().addActionListener(
                e -> {
                    s.faireInteraction(1);
                }
        );
        vs.getSondage_plus_proche().addActionListener(
                e -> {
                    s.faireInteraction(2);
                }
        );
        vs.getSondage_plus_utile().addActionListener(
                e -> {
                    s.faireInteraction(3);
                }
        );
        vs.getSondage_plus_utile_proportionnel().addActionListener(
                e -> {
                    s.faireInteraction(4);
                }
        );
        vs.getInteractionRetourBtn().addActionListener(
                e -> {
                    vs.setContentPane(vs.getChoicePane());
                    vs.update();
                }
        );
    }

    public JPanel getMainPanel() {
        return mainPanel;
    }
}
