import systeme_electoral.SystemeElectoral;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        //Définir la classe principale
        SystemeElectoral s = new SystemeElectoral("config.txt");

        //Appeler la fenêtre graphique
        VueSystemeElectoral vs = new VueSystemeElectoral(s);
        s.addObserver(vs);
        vs.setVisible(true);
        Controlleur c = new Controlleur(s,vs);
    }

}
