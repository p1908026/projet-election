
import systeme_electoral.SystemeElectoral;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class VueSystemeElectoral extends JFrame implements Observer {
    private static final int PIXEL_PER_SQUARE = 60;

    private JComponent currentComponent;
    private final SystemeElectoral systemeElectoral;
    private final JPanel startPane;
    private final JPanel choicePane;
    private final JPanel scrutinChoicePane;
    private final JPanel interactionChoicePane;
    private JButton startBtn,
            scrutinBtn,interactionBtn,retourBtn,
            scrutin_en_un_tour,scrutin_en_deux_tours,vote_approbation,vote_alternatif,methode_borda,scrutinRetourBtn,
            interactions_sociopolitiques,sondage_plus_proche,sondage_plus_utile,sondage_plus_utile_proportionnel,interactionRetourBtn;

    private final boolean isPressed = false;

    /**
     * Création des boutons à afficher et formatage de la fenêtre
     * @param systemeElectoral Système éléctoral général
     */
    public VueSystemeElectoral(SystemeElectoral systemeElectoral) {
        int size = 10;
        this.systemeElectoral = systemeElectoral;
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(size * PIXEL_PER_SQUARE, (size+1) * PIXEL_PER_SQUARE);

        startPane = new JPanel(new GridLayout(10,10));
        choicePane = new JPanel(new GridLayout(10,10));
        scrutinChoicePane = new JPanel(new GridLayout(10,10));
        interactionChoicePane = new JPanel(new GridLayout(10,10));

        initStartPane(startPane);
        initChoicePane(choicePane);
        initScrutinChoicePane(scrutinChoicePane);
        initInteractionChoicePane(interactionChoicePane);
    }

    /**
     * Initialisation des boutons principaux
     * @param startPane Panel
     */
    public void initStartPane(JPanel startPane) {
        JLabel startLabel = new JLabel("Bienvenue à l'élection électorale 2022 !");
        startLabel.setHorizontalAlignment(JLabel.CENTER);
        startBtn = new JButton("Commencer");
        startPane.add(startLabel);
        startPane.add(startBtn);
    }

    /**
     * Initialisation des boutons de choix d'option
     * @param choicePane Panel
     */
    public void initChoicePane(JPanel choicePane) {
        JLabel infosPopulation = new JLabel("Nombre de candidats : " + systemeElectoral.getNBCandidats() +
                "   Nombre d'électeurs : " + systemeElectoral.getNBElecteurs());
        JLabel startLabel = new JLabel("Choississez votre action");
        infosPopulation.setHorizontalAlignment(JLabel.CENTER);
        startLabel.setHorizontalAlignment(JLabel.CENTER);
        scrutinBtn = new JButton("Faire un scrutin");
        interactionBtn = new JButton("Faire une interaction");
        retourBtn = new JButton("Retour");
        choicePane.add(infosPopulation);
        choicePane.add(startLabel);
        choicePane.add(scrutinBtn);
        choicePane.add(interactionBtn);
        choicePane.add(retourBtn);
    }

    /**
     * Initialisation des boutons de scrutin
     * @param scrutinChoicePane Panel
     */
    public void initScrutinChoicePane(JPanel scrutinChoicePane) {
        JLabel type_de_scrutin = new JLabel("Type de scrutin");
        type_de_scrutin.setHorizontalAlignment(JLabel.CENTER);
        scrutin_en_un_tour = new JButton("Scrutin en un tour");
        scrutin_en_deux_tours = new JButton("Scrutin en deux tours");
        vote_approbation = new JButton("Vote approbation");
        vote_alternatif = new JButton("Vote alternatif");
        methode_borda = new JButton("Methode Borda");
        scrutinRetourBtn = new JButton("Retour");
        scrutinChoicePane.add(type_de_scrutin);
        scrutinChoicePane.add(scrutin_en_un_tour);
        scrutinChoicePane.add(scrutin_en_deux_tours);
        scrutinChoicePane.add(vote_approbation);
        scrutinChoicePane.add(vote_alternatif);
        scrutinChoicePane.add(methode_borda);
        scrutinChoicePane.add(scrutinRetourBtn);
    }

    /**
     * Initialisation des boutons de sondage
     * @param interactionChoicePane Panel
     */
    public void initInteractionChoicePane(JPanel interactionChoicePane) {
        JLabel type_d_interactions = new JLabel("Type d'interactions");
        type_d_interactions.setHorizontalAlignment(JLabel.CENTER);
        interactions_sociopolitiques = new JButton("Interactions socio-politiques");
        sondage_plus_proche = new JButton("Sondage de déplacement vers le plus proche");
        sondage_plus_utile = new JButton("Sondage de déplacement vers le plus utile");
        sondage_plus_utile_proportionnel = new JButton("Sondage de déplacement vers le plus utile proportionnellement à tous");
        interactionRetourBtn = new JButton("Retour");
        interactionChoicePane.add(type_d_interactions);
        interactionChoicePane.add(interactions_sociopolitiques);
        interactionChoicePane.add(sondage_plus_proche);
        interactionChoicePane.add(sondage_plus_utile);
        interactionChoicePane.add(sondage_plus_utile_proportionnel);
        interactionChoicePane.add(interactionRetourBtn);
    }

    public JPanel getStartPane() {
        return startPane;
    }

    public JPanel getChoicePane() {
        return choicePane;
    }

    public JPanel getScrutinChoicePane() {
        return scrutinChoicePane;
    }

    public JPanel getInteractionChoicePane() {
        return interactionChoicePane;
    }

    public JButton getStartBtn() {
        return startBtn;
    }

    public JButton getScrutinBtn() {
        return scrutinBtn;
    }

    public JButton getInteractionBtn() {
        return interactionBtn;
    }

    public JButton getRetourBtn() {
        return retourBtn;
    }

    public JButton getScrutin_en_un_tour() {
        return scrutin_en_un_tour;
    }

    public JButton getScrutin_en_deux_tours() {
        return scrutin_en_deux_tours;
    }

    public JButton getVote_approbation() {
        return vote_approbation;
    }

    public JButton getVote_alternatif() {
        return vote_alternatif;
    }

    public JButton getMethode_borda() {
        return methode_borda;
    }

    public JButton getScrutinRetourBtn() {
        return scrutinRetourBtn;
    }

    public JButton getInteractions_sociopolitiques() {
        return interactions_sociopolitiques;
    }

    public JButton getSondage_plus_proche() {
        return sondage_plus_proche;
    }

    public JButton getSondage_plus_utile() {
        return sondage_plus_utile;
    }

    public JButton getSondage_plus_utile_proportionnel() {
        return sondage_plus_utile_proportionnel;
    }

    public JButton getInteractionRetourBtn() {
        return interactionRetourBtn;
    }

    @Override
    public void update(Observable o, Object arg) {
        repaint();
    }

    public void update() {
        revalidate();
        repaint();
    }
}
